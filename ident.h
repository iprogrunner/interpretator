#include <iostream>
#include <string>

using namespace std;

#pragma once

class Ident{
public:
	enum ident_type{INT, REAL};
private:
	string name;
	bool assign;
	ident_type type;
	int i_value;
	double r_value;
public:
	Ident(string n,ident_type t);
	string get_name();
	bool is_assign();
	ident_type get_type();
	void set_assign();
	template<class T> T get_value();
	template<class T> void set_value(T val);
};
