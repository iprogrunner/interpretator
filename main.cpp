#include "executer.h"

int main(int argc, char** argv){
	FILE *fp1, *fp2;
	bool outp_redirected = false;
	if (argc < 2){
		cout<< "Wrong number of arguments!" << endl;
		return 1;
	}
	if ((fp1 = freopen(argv[1],"r",stdin))==NULL){
		cout << "Wrong input file!" << endl;
		return 2;
	}
	if (argc > 2){
		if ((fp2 = freopen(argv[2],"w",stdout))==NULL){
                	cout << "Cant write in this file!" << endl;
			cout << "Output redirected to standart" << endl;
		}
		else outp_redirected = true;
	}
	Parser parser;
	parser.analyze();
	parser.tid_out();
	Executer ex;
	ex.execute(parser);
	parser.tid_out();
	fclose(fp1);
	if (outp_redirected) fclose(fp2);
}
