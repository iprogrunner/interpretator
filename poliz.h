#include <iostream>
#include <vector>
#include "lex.h"

using namespace std;

class Poliz{
	vector<Lex> p;
public:
	void put_lex(Lex l);
	void put_lex(Lex l,int place);
	int get_free();
	void print();
	Lex get_el(int i);
};
