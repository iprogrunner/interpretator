#include "poliz.h"

using namespace std;

void Poliz::put_lex(Lex l){p.push_back(l);}

void Poliz::put_lex(Lex l, int place){p[place]=l;}

int Poliz::get_free(){return p.size();}

void Poliz::print(){
	for (int i=0;i<p.size();i++) cout << i << ' ' << p[i];
}

Lex Poliz::get_el(int i){ return p[i];}
