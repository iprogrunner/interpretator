#include "scaner.h"
#include <algorithm>
#include <cstring>

void Scaner::fill_TW(){
	TW.push_back("and");
	TW.push_back("else");
	TW.push_back("if");
	TW.push_back("int");
	TW.push_back("not");
	TW.push_back("or");
	TW.push_back("program");
	TW.push_back("real");
	TW.push_back("then");
	TW.push_back("while");
}

void Scaner::fill_TD(){
	TD.push_back(";");
	TD.push_back("@");
	TD.push_back(",");
	TD.push_back("{");
	TD.push_back("}");
	TD.push_back("=");
	TD.push_back("(");
	TD.push_back(")");
	TD.push_back("==");
	TD.push_back("<");
	TD.push_back(">");
	TD.push_back("+");
	TD.push_back("-");
	TD.push_back("*");
	TD.push_back("/");
	TD.push_back("%");
	TD.push_back("<=");
	TD.push_back(">=");
	TD.push_back("!=");
}

Scaner::Scaner():TW(10),TD(19){
	fill_TW();
	fill_TD();
	cs = H; 
}

bool Scaner::is_alpha(char c){
	return ((c>='a')&&(c<='z'))||((c>='A')&&(c<='Z'));
}

bool Scaner::is_digit(char c){
	return (c>='0')&&(c<='9');
}

bool Scaner::compare(vector<string>::iterator b,vector<string>::iterator e, string val){
	while (b!=e){
		if (!strcmp(b->c_str(),val.c_str())) return true;
		else b++;
	}
	return false;
}

int Scaner::str = 1;
int Scaner::pos = 1;

Lex Scaner::get_lex(){
	char c;
	string idn;
	if (cs == H){
		c = getchar();//cin >> c;
		pos++;
		if (c==EOF) return Lex(Lex::LEX_FIN,"Unexpected end of file",pos,str);
	}
	else{
		c = temp;
		cs = H;
	}
	while (true){
		//cout << c << ' ' << (int)c << endl;
		switch(cs){
		case H:
			//cout << "H" << endl;
			if((c==' ')||(c=='\r')||(c=='\t'));
			else if (c=='\n'){
				str++;
				pos = 1;
			}
			else if (is_alpha(c)){
				idn.clear();
				idn += c;
				cs = IDENT;
			}
			else if (is_digit(c)){
				idn.clear();
				idn +=c;
				cs = NUMB_INT;
			}
			else if ((c=='=')||(c=='<')||(c=='>')){
				idn.clear();
				idn += c;
				cs = ALE;
			}
			else if (c=='@') return Lex(Lex::LEX_FIN,"@",pos,str);
			else if (c=='!') cs = NEQ;
			else if (c=='/') cs = MB_COM;
			else{
				cs = DELIM;
				idn.clear();
				idn += c;
			}
			break;
		case IDENT:
			//cout << "ident" << endl;
			if (is_alpha(c)||is_digit(c)) idn += c;
			else{
				temp = c;
				if (!compare(TW.begin(),TW.end(),idn)){
					//TID.push_back(idn);
					return Lex(Lex::LEX_ID,idn,pos,str);
				}
				else return Lex(Lex::LEX_LINKWORD,idn,pos,str);
			}
			break;
		case NUMB_INT:
			//cout << "numb" << endl;
			if (is_digit(c)) idn += c;
			else if (c=='.'){
				idn+=c;
				cs = NUMB_REAL;
			}
			else{
				temp = c;
				return Lex(Lex::LEX_NUM_INT,idn,pos,str);
			}
			break;
		case NUMB_REAL:
			if (is_digit(c)) idn+=c;
			else{
				temp = c;
				return Lex(Lex::LEX_NUM_REAL,idn,pos,str);
			}
			break;
		case MB_COM:
			if (c == '*') cs = COM;
			else{
				temp = c;
				return Lex(Lex::LEX_PUNCT,"/",pos,str);
			}
			break;
		case COM:
			//cout << "com" << endl;
			if (c=='*') cs = MB_NOT_COM;
			else if (c=='@'){
				cs = H;
				return Lex(Lex::LEX_ERR,"Unexpected symbol in comment",pos,str);
			}
			break;
		case MB_NOT_COM:
			if (c=='/') cs = H;
			else if (c=='*');
			else cs = COM;
			break;
		case ALE:
			//cout << "ale" << endl;
			if (c=='='){
				idn += c;
				cs = H;
				return Lex(Lex::LEX_PUNCT,idn,pos,str);
			}
			else{
				temp = c;
				return Lex(Lex::LEX_PUNCT,idn,pos,str);
			}
			break;
		case NEQ:
			//cout << "neq" << endl;
			if (c=='='){
				cs = H;
				return Lex(Lex::LEX_PUNCT,"!=",pos,str);
			}
			else{
				cs = H;
				return Lex(Lex::LEX_ERR,"Unexpected symbol after '!'",pos,str);
			}
			break;
		case DELIM:
			//cout << "delim" << endl;
                        if (compare(TD.begin(),TD.end(),idn)){
				temp = c;
                                return Lex(Lex::LEX_PUNCT,idn,pos,str);
                        }
                        else{
				cs = H;
				return Lex(Lex::LEX_ERR,"Unexpected symbol "+idn,pos,str);
			}
		}
		c = getchar();//cin >> c;
		pos++;
		if (c==EOF) return Lex(Lex::LEX_FIN,"Unexpected end of file",pos,str);
	}
}
	
