#include "ident.h"

template<class T> T Ident::get_value(){
        //cout << i_value << ' ' << r_value << endl;
	switch(type){
                case INT: return i_value;
                case REAL: return r_value;
        }
}

template<class T> void Ident::set_value(T val){
        assign = true;
        switch(type){
                case INT: i_value = val; break;
                case REAL: r_value = val; break;
        }
}
