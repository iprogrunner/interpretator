#include "lex.h"
#include <sstream>

using namespace std;

Lex::Lex(type_of_lex t, string v, int posit, int strng){
	type = t;
	value = v;
	str = strng;
	pos = posit;
}

Lex::type_of_lex Lex::get_type(){return type;}

string Lex:: get_value(){return value;}

string Lex::get_pos(){
	string str_1,str_2;
    	stringstream ss,ss1;
    	ss<<str;
    	getline(ss, str_1);
	ss1<<pos;
	getline(ss1, str_2);
	return "("+str_1+","+str_2+")";
}

int Lex::get_pos_int(){return pos;}

ostream &operator<<(ostream &s,/*const*/ Lex &l){
	switch(l.type){
		case Lex::LEX_NULL: s<<"LEX_NULL";break;
		case Lex::LEX_LINKWORD: s<<"LEX_LINKWORD";break;
		case Lex::LEX_PUNCT: s<<"LEX_PUNCT";break;
		case Lex::LEX_NUM_INT: s<<"LEX_NUM_INT";break;
		case Lex::LEX_NUM_REAL: s<<"LEX_NUM_REAL"; break;
		case Lex::LEX_ID: s<<"LEX_ID";break;
		case Lex::POLIZ_LABEL: s<<"POLIZ_LABEL to "<< l.get_pos();break;
		case Lex::POLIZ_ADDRESS: s<<"POLIZ_ADDRESS";break;
		case Lex::POLIZ_GO: s<<"POLIZ_GO";break;
		case Lex::POLIZ_FGO: s<<"POLIZ_FGO";break;
		case Lex::LEX_ERR: s<<"LEX_ERR";break;
		case Lex::LEX_FIN: s<<"LEX_FIN";break;
	}
	s<< "(" << l.value << ")" << endl;
	return s;
}
