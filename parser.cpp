#include "parser.h"
#include <cstdlib>
#include <cstring>
#include <sstream>
#include "ident_template.cpp"

void Parser::tid_out(){
	list<Ident>::iterator it;
	for (it=TID.begin();it!=TID.end();it++){
		cout << it->get_name()<< ' ';
		if (it->get_type()==Ident::INT)
			cout << it->get_value<int>();
		else cout << it->get_value<double>();
		cout << endl;
	}
}

Ident::ident_type Parser::pop(){
        Ident::ident_type t=type_stack.top();
        type_stack.pop();
        return t;
}


void Parser::stack_out(){
	if (!type_stack.empty()){
		Ident::ident_type t=pop();
		if (t== Ident::INT) cout<< "INT"<<endl;
		else cout << "REAL" <<endl;
		stack_out();
		type_stack.push(t);
	}
}

list<Ident>::iterator Parser::find_tid(string temp){
	list<Ident>::iterator p;
        for(p=TID.begin();p!=TID.end();p++)
                if (!strcmp(temp.c_str(),p->get_name().c_str()))
                        return p;
	return p;

}

list<Ident>::iterator Parser::push_tid(Ident ident){
	list<Ident>::iterator p = find_tid(ident.get_name());
        if (p == TID.end()){
		TID.push_back(ident);
		return --(TID.end());
	}
	else return p;
}

bool Parser::is_error(){return err;}

void Parser::gl(){
	curr_lex = scaner.get_lex();
	//cout << curr_lex;
	//stack_out();
	if (curr_lex.get_type()==Lex::LEX_ERR){
		cout << curr_lex.get_pos() << curr_lex.get_value() << endl;
		err = true;
	}
	//if (curr_lex.get_type()==Lex::LEX_FIN) exit(0);
}

void Parser::analyze(){
	err = false;
	gl();
	P();
}

void Parser::P(){/*program*/
	//cout << "P" << endl;
	if (curr_lex.get_value().compare("program")){
		cout << "Expected program at the begining" << endl;
		err = true;
	}
	else gl();
	if (curr_lex.get_value().compare("{")){
		cout << curr_lex.get_pos() << " { expected" << endl;
		err = true;
	}
	else gl();
	D1();
	B();
	gl();
	if (curr_lex.get_type() != Lex::LEX_FIN){
		cout << curr_lex.get_pos() << "End of a program expected" << endl;
		err = true;
	}
	else prog.put_lex(curr_lex);
}

void Parser::D1(){/*declarations*/
	//cout << "D1" << endl;
	Ident::ident_type t;
	while (!curr_lex.get_value().compare("int")||!curr_lex.get_value().compare("real")){
		if (!curr_lex.get_value().compare("int")) t = Ident::INT;
		else t = Ident::REAL;
		gl();
		D(t);
		while (!curr_lex.get_value().compare(",")){
			gl();
			D(t);
		}
		if (curr_lex.get_value().compare(";")){
                	cout << curr_lex.get_pos() << " ; expected" << endl;
			err = true;
		}
        	else gl();
	}
}

void Parser::D(Ident::ident_type t){/*declaration*/
	//cout << "D" << endl;
	if (curr_lex.get_type() != Lex::LEX_ID){
		cout << curr_lex.get_pos() << "Indificator expected" << endl;
		err = true;
	}
	else{
		list<Ident>::iterator it = push_tid(Ident(curr_lex.get_value(),t));
		gl();
		if (!curr_lex.get_value().compare("=")){
			gl();
			if (((curr_lex.get_type() == Lex::LEX_NUM_INT)&&(t==Ident::INT))||
			((curr_lex.get_type() == Lex::LEX_NUM_REAL)&&(t==Ident::REAL))){
				if (t==Ident::INT){
					istringstream ss(curr_lex.get_value());
					int val1;
					ss >> val1;
					it->set_value(val1);
				}
				else{
    					double val = atof(curr_lex.get_value().c_str());
					it->set_value(val);
				}
			
			}
			else{
				cout << curr_lex.get_pos() << "Wrong assigment" << endl;
				err = true;
			}
			gl();
		}
	}
}

void Parser::B(){/*operators*/
	//cout << "B" << endl;
	while (curr_lex.get_value().compare("}")&&(curr_lex.get_type()!=Lex::LEX_FIN)){
		S();
		gl();
	}
}

void Parser::S(){/*operator*/
	//cout << "S" << endl;
	int pl0,pl1;
	if (!curr_lex.get_value().compare("if")){
		gl();
		if (curr_lex.get_value().compare("(")){
                        cout << curr_lex.get_pos() << "( expected" << endl;
			err = true;
		}
		gl();
		E();
		if (pop()!=Ident::INT){
			cout<< curr_lex.get_pos() << "Wrong expression for if"<< endl;
			err = true;
		}
		pl0 = prog.get_free();
		prog.put_lex(Lex(Lex::LEX_NULL));
		prog.put_lex(Lex(Lex::POLIZ_FGO));
		if (curr_lex.get_value().compare(")")){
                        cout << curr_lex.get_pos() << ") expected" << endl;
			err = true;
		}
		gl();
		S();
		pl1 = prog.get_free();
		prog.put_lex(Lex(Lex::LEX_NULL));
		prog.put_lex(Lex(Lex::POLIZ_GO));
		prog.put_lex(Lex(Lex::POLIZ_LABEL,"",prog.get_free()),pl0);
		gl();
		if (!curr_lex.get_value().compare("else")){
			gl();
			S();
			prog.put_lex(Lex(Lex::POLIZ_LABEL,"",prog.get_free()),pl1);
		}
		else{
			cout << curr_lex.get_pos() << "else expected" << endl;
			err = true;
		}
	}
	else if(!curr_lex.get_value().compare("while")){
		pl0 = prog.get_free();
		gl();
		if (curr_lex.get_value().compare("(")){
                        cout << curr_lex.get_pos() << "( expected" << endl;
			err = true;
		}
		gl();
		E();
		if (pop()!=Ident::INT){
                        cout<< curr_lex.get_pos() << "Wrong expression for if"<< endl;
			err = true;
		}
		pl1 = prog.get_free();
		prog.put_lex(Lex(Lex::LEX_NULL));
		prog.put_lex(Lex(Lex::POLIZ_FGO));
		if (curr_lex.get_value().compare(")")){
                        cout << curr_lex.get_pos() << ") expected" << endl;
			err = true;
		}
		gl();
		S();
		prog.put_lex(Lex(Lex::POLIZ_LABEL,"",pl0));
		prog.put_lex(Lex(Lex::POLIZ_GO));
		prog.put_lex(Lex(Lex::POLIZ_LABEL,"",prog.get_free()),pl1);
	}
	else if(curr_lex.get_type() == Lex::LEX_ID){
		list<Ident>::iterator p=find_tid(curr_lex.get_value());
                if (p!=TID.end()){
			p->set_assign();
                        type_stack.push(p->get_type());//?
                        prog.put_lex(Lex(Lex::POLIZ_ADDRESS,p->get_name()));
                }
		else{
			cout << curr_lex.get_pos() << "Undeclared identeficator" << endl;
			err = true;
			TID.push_back(Ident(curr_lex.get_value(),Ident::INT));
                	type_stack.push(Ident::INT);
		}
		gl();
		if (curr_lex.get_value().compare("=")){
			cout << curr_lex.get_pos() << "Assign expected" << endl;
			err = true;
		}
		gl();
		E();
		pop();pop();
		prog.put_lex(Lex(Lex::LEX_PUNCT,"="));
		if (curr_lex.get_value().compare(";")){
			cout << curr_lex.get_pos() << "; expected" << endl;
			err = true;
		}
	}
	else if(!curr_lex.get_value().compare("{")){
		gl();
		B();
		if (curr_lex.get_value().compare("}")){ 
                        cout<< curr_lex.get_pos() << "} expected" << endl;
			err = true;
		}
	}
}

void Parser::E(){/*expression*/
	//cout << "E" << endl;
	E1();
	string s = curr_lex.get_value();
	if (!s.compare("==")||!s.compare("<")||!s.compare(">")||
		!s.compare("<=")||!s.compare(">=")||!s.compare("!=")){
		gl();
		E1();
		if (pop()!=pop()){ 
			cout<<curr_lex.get_pos() << "Wrong expression for comparation" << endl;
			err = true;
		}
		type_stack.push(Ident::INT);
		prog.put_lex(Lex(Lex::LEX_PUNCT,s));
		s = curr_lex.get_value();
	}
}

void Parser::E1(){/*multiplier*/
	//cout << "E1" << endl;
	T();
	string s = curr_lex.get_value();
	while(!s.compare("+")||!s.compare("-")||!s.compare("or")){
		gl();
		T();
		Ident::ident_type t = pop();
		if ((pop()!=Ident::REAL)&&(t!=Ident::REAL)) type_stack.push(Ident::INT);
                else if (s.compare("or")) type_stack.push(Ident::REAL);
                else{
			cout << curr_lex.get_pos() <<"Wrong expression for or" << endl;
			err = true;
			type_stack.push(Ident::INT);
		}
		prog.put_lex(Lex(Lex::LEX_PUNCT,s));
		s = curr_lex.get_value();
	}
}

void Parser::T(){/*additor*/
	//cout << "T" << endl;
	F();
	string s = curr_lex.get_value();
        while(!s.compare("*")||!s.compare("/")||!s.compare("and")||!s.compare("%")){
                gl();
                F();
                Ident::ident_type t = pop();
                if ((pop()!=Ident::REAL)&&(t!=Ident::REAL)) type_stack.push(Ident::INT);
		else if (s.compare("and")&&s.compare("%")) type_stack.push(Ident::REAL);
		else{
			cout <<curr_lex.get_pos() << "Wrong expression for and" << endl;
			err = true;
			type_stack.push(Ident::INT);
		}
		prog.put_lex(Lex(Lex::LEX_PUNCT,s));
		s = curr_lex.get_value();
        }
}

void Parser::F(){
	//cout << "F " << curr_lex << endl;
	if (curr_lex.get_type() == Lex::LEX_ID){
		list<Ident>::iterator p=find_tid(curr_lex.get_value());
		if (p!=TID.end()){
			type_stack.push(p->get_type());
			if (!p->is_assign()){
				cout << curr_lex.get_pos() << "Unassigned variable" << endl;
				err = true;
			}
			prog.put_lex(curr_lex);
		}
		else{ 
			cout << curr_lex.get_pos() << "Undeclared identeficator" << endl;
			err = true;
			TID.push_back(Ident(curr_lex.get_value(),Ident::INT));
			type_stack.push(Ident::INT);
		}
		gl();
	}
	else if (curr_lex.get_type() == Lex::LEX_NUM_INT){
		type_stack.push(Ident::INT);
		prog.put_lex(curr_lex);
		gl();
	}
	else if (curr_lex.get_type()==Lex::LEX_NUM_REAL){
		type_stack.push(Ident::REAL);
		prog.put_lex(curr_lex);
		gl();
	}
	else if (!curr_lex.get_value().compare("not")){
		gl();
		F();
		if (pop()!=Ident::INT){
			cout<< curr_lex.get_pos() <<"Wrong expression for not" << endl;
			err = true;
		}
		else prog.put_lex(curr_lex);
	}
	else if (!curr_lex.get_value().compare("(")){
		gl();
		E();
		if (curr_lex.get_value().compare(")")){
			cout << curr_lex.get_pos() << ") expected" << endl;
			err = true;
		}
		else gl();
	}
	else{
		cout << curr_lex.get_pos() << "Unexpected lexem" << endl;
		err = true;
	}
}
