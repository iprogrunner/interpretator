#include "ident.h"

Ident::Ident(string n,Ident::ident_type t){
	name = n;
	assign  = false;
	type = t;
}

string Ident::get_name(){return name;}

bool Ident::is_assign(){return assign;}

Ident::ident_type Ident::get_type(){return type;}

void Ident::set_assign(){ assign = true;}
