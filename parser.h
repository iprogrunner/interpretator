#include "scaner.h"
#include "ident.h"
#include "poliz.h"
#include <list>
#include <stack>

class Parser{
	Scaner scaner;
	Lex curr_lex;
	list<Ident> TID;
	stack<Ident::ident_type> type_stack;
	bool err;
	
	void stack_out();	
	Ident::ident_type pop();
	list<Ident>::iterator push_tid(Ident ident);
		
	void gl();	
	void P();
	void D1();
	void D(Ident::ident_type t);
	void B();
	void S();
	void E();
	void E1();
	void T();
	void F();
	
public:
	Poliz prog;
	bool is_error();
	list<Ident>::iterator find_tid(string temp);
	void analyze();
	void tid_out();
};
