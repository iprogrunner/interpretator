#include "lex.h"
#include <vector>

class Scaner{
	enum state{H, IDENT, NUMB_INT, NUMB_REAL, MB_COM, COM, MB_NOT_COM, ALE, DELIM, NEQ};
	state cs;
	vector<string> TW;
	vector<string> TD;
	static int pos;
	static int str;
	char temp;
	void fill_TW();
	void fill_TD();
	bool is_digit(char c);
	bool is_alpha(char c);
	bool compare(vector<string>::iterator b,vector<string>::iterator e, string val);
public:
	Scaner();
	Lex get_lex();
};
