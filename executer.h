#include "parser.h"
#include <stack>

class Executer{
	Lex pc_el;
	stack<int> args_int;
	stack<double> args_real;
	stack<string> args_idn;
	bool is_int;

	void push(int val);
	void repush();
	int pop_int();
	double pop_real();
public:
	void execute(Parser &parser);
};
