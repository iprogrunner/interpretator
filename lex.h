#include <iostream>
#include <string>

using namespace std;

#pragma once

class Lex{
public:
	enum type_of_lex{LEX_NULL,LEX_LINKWORD,LEX_PUNCT,LEX_NUM_INT,LEX_NUM_REAL,LEX_ID,
	POLIZ_LABEL,POLIZ_ADDRESS,POLIZ_GO,POLIZ_FGO,LEX_ERR,LEX_FIN};
private:
	type_of_lex type;
	string value;
	int str;
	int pos;
public:
	Lex(type_of_lex t=LEX_NULL, string v="", int posit=0,int strng=0);
	type_of_lex get_type();
	string get_value();
	string get_pos();
	int get_pos_int();
	friend ostream &operator<<(ostream &s, /*const*/ Lex &l);
};
