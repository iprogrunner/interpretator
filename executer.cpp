#include "executer.h"
#include <cstdlib>

using namespace std;

void Executer::push(int val){
	if (is_int) args_int.push(val);
        else args_real.push(val);
}

void Executer::repush(){
	is_int = false;
	if (!args_int.empty()){
		int tmp = pop_int();
		repush();
		args_real.push(tmp);
	}
}

int Executer::pop_int(){
	int res = args_int.top();
	args_int.pop();
	return res;
}

double Executer::pop_real(){
	double res = args_real.top();
	args_real.pop();
	if (args_real.empty()) is_int = true;
	return res;
}

void Executer::execute(Parser &parser){
	int tmp;
	double k;
	is_int = true;
	list<Ident> ::iterator p;
	for (int i=0;i<parser.prog.get_free();i++){
		pc_el = parser.prog.get_el(i);
		//cout << args_int.size() << ' ' << args_real.size() << endl;
		//parser.tid_out();
		if (!args_int.empty()) cout << args_int.top() << endl;
		cout << pc_el;
		switch (pc_el.get_type()){
			case Lex::LEX_NUM_INT:
				push(atoi(pc_el.get_value().c_str()));
				break;
			case Lex::LEX_NUM_REAL:
				repush();
				args_real.push(atof(pc_el.get_value().c_str()));
				break;
			case Lex::POLIZ_ADDRESS:
				args_idn.push(pc_el.get_value());
				break;
			case Lex::POLIZ_LABEL:
				args_int.push(pc_el.get_pos_int());
				break;
			case Lex::LEX_ID:
				p = parser.find_tid(pc_el.get_value());
				if (p->get_type() == Ident::REAL){
					repush();
					args_real.push(p->get_value<double>());
				}
				else push(p->get_value<int>());
				break;
			case Lex::POLIZ_GO:
				i = pop_int()-1;
				break;
			case Lex::POLIZ_FGO:
				tmp = pop_int();
				if (!pop_int())	i = tmp - 1;
				break;
			case Lex::LEX_PUNCT:
				string symb = pc_el.get_value();
				if (!symb.compare("not"))
					push(!pop_int());
				else if (!symb.compare("or")){
					tmp = pop_int();
                                	push(pop_int()||tmp);
				}
				else if (!symb.compare("and")){
					tmp = pop_int();
                                	push(pop_int()&&tmp);
				}
				else if (!symb.compare("+")){
					if (is_int) push(pop_int()+pop_int());
					else{
						args_real.push(pop_real()+pop_real());
						is_int = false;
					}
				}
				else if (!symb.compare("*")){
                                        if (is_int) push(pop_int()*pop_int());
                                        else{
						args_real.push(pop_real()*pop_real());
						is_int = false;
					}
                                }
				else if (!symb.compare("-")){
                                        if (is_int){
						tmp=pop_int();
						push(pop_int()-tmp);
					}
                                        else{
						k = pop_real();
						args_real.push(pop_real()-k);
						is_int = false;
					}
                                }
				else if (!symb.compare("%")){
                                        tmp = pop_int();
					push(pop_int()%tmp);
				}
				else if (!symb.compare("/")){
                                        if (is_int){
						tmp = pop_int();
						if (tmp!=0) push(pop_int()/tmp);
						else{
							cout << "Divide by zero error" << endl;
							return;
						}
					}
                                        else{
						k = pop_real();
                                                if (tmp!=0){
							args_real.push(pop_real()/k);
							is_int = false;
						}
                                                else{ 
                                                        cout << "Divide by zero error" << endl;
                                                        return;
                                                }
                                	}
				}
				else if (!symb.compare("==")){
                                        if (is_int) args_int.push(pop_int()==pop_int());
					else  args_int.push(pop_real()==pop_real());
				}
				else if (!symb.compare("<")){
                                      	if (is_int){ 
						tmp = pop_int();
						args_int.push(pop_int()<tmp);
					}
					else{
						k = pop_real();
						args_int.push(pop_real()<k);
					}
                                }
				else if (!symb.compare(">")){
                                        if (is_int){
                                                tmp = pop_int();
                                                args_int.push(pop_int()>tmp);
                                        }
                                        else{  
                                                k = pop_real();
                                                args_int.push(pop_real()>k);
                                        }

                                }
				else if (!symb.compare("<=")){
                                        if (is_int){
                                                tmp = pop_int();
                                                args_int.push(pop_int()<=tmp);
                                        }
                                        else{  
                                                k = pop_real();
                                                args_int.push(pop_real()<=k);
                                        }

                                }
				else if (!symb.compare(">=")){
                                        if (is_int){
                                                tmp = pop_int();
                                                args_int.push(pop_int()>=tmp);
                                        }
                                        else{  
                                                k = pop_real();
                                                args_int.push(pop_real()>=k);
                                        }

                                }
				else if (!symb.compare("!=")){
                                        if (is_int) args_int.push(pop_int()!=pop_int());
					else  args_int.push(pop_real()!=pop_real());
                                }
				else if (!symb.compare("=")){
					p = parser.find_tid(args_idn.top());
					args_idn.pop();
                                        if (is_int) p->set_value<int>(pop_int());
                                        else p->set_value<double>(pop_real());
                                }
				break;
			//default: cout << "Poliz unexpected lexem" << endl;break;
		}
	}
	cout << "Finish of executing!" << endl;
}
